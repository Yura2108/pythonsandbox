import json
import os
import shutil
import subprocess
import time
from pathlib import Path

from aiohttp import web
from aiohttp.web_request import Request


EXECUTION_FILE_NAME = "main.py"
INPUT_LINES_HEADER_NAME = "INPUT_LINES"


def write_content_and_get_abs_path(file_path: str, content: str) -> str:
    with open(file_path, "w") as file:
        file.write(content)
    return str(Path(file_path).absolute())


def get_code_input(request: Request) -> str | None:
    if INPUT_LINES_HEADER_NAME in request.headers and isinstance(request.headers[INPUT_LINES_HEADER_NAME], str):
        return request.headers[INPUT_LINES_HEADER_NAME].replace("\\n", "\n")
    return None


async def handle(request: Request):
    if not request.can_read_body:
        return web.Response(status=402)
    # Входные данные ожидаются в виде Plain Text
    content = await request.text()

    # Используем текущее время как название директории исполнения
    directory_name = str(time.time())
    os.mkdir(directory_name)

    # Записали код и получили входные данные
    absolute_file_path = write_content_and_get_abs_path(directory_name + os.sep + EXECUTION_FILE_NAME, content)
    code_input = get_code_input(request)

    # Запуск
    process = subprocess.run(["python",EXECUTION_FILE_NAME], capture_output=True, text=True,
                             input=code_input, cwd=directory_name)

    # Чистим директорию
    shutil.rmtree(directory_name)

    if process.returncode == 0:
        response = {"ok": True, "result": process.stdout}
    else:
        # p.s. Чтобы убрать из stacktrace ошибок абсолютный путь к файлу делаем замену
        response = {"ok": False, "result": process.stderr.replace(absolute_file_path, EXECUTION_FILE_NAME)}
    response = json.dumps(response)
    return web.Response(text=response)


app = web.Application()
app.router.add_get('/run_code', handle)

web.run_app(app, port=80)
