# PythonSandBox

**Сервис для безопасного и изолированного выполнения Python кода**

## Запуск

С помощью **docker-compose.yml**
```commandline
docker-compose up -d
```

С помощью **Docker**
```commandline
docker build -t pythonsandbox PythonSandBox
docker run -it pythonsandbox -p 1111:80
```

## Использование

### Для взаимодействия с сервисом используйте HTTP запросы на **URL**: [127.0.0.1:1111/run_code](127.0.0.1:1111/run_code)

### В Body передается код на выполнение.
#### Чтобы передать входные данные используйте header **INPUT_LINES**

### Простейший пример:
Выполнить код
```python
a = int(input())
b = int(input())
print(a ** 2 + b)
```

Входные данные:
```text
123
12
```

#### Пример запроса к сервису

```commandline
curl --location --request GET '127.0.0.1:1111/run_code' \
--header 'INPUT_LINES: 123\n12' \
--header 'Content-Type: text/plain' \
--data 'a = int(input())
b = int(input())
print(a ** 2 + b)'
```

### Результат выполнения программы
```json
{"ok": true, "result": "15141\n"}
```

### В случае возникновения ошибки
```json
{"ok": false, "result": "File \"main.py\", line 3\nprint(a ** 2 +` b)\n^\nSyntaxError: invalid syntax\n"}
```


## Обратите внимание, что код передаётся как Plain Text с соблюдением табуляции иначе он не будет выполнен

# Момент безопасности

### Так как весь код выполняется в Docker контейнере он не может нанести вред системе, из которой был запущен.
